package org.budo.warehouse.logic.api;

import org.budo.warehouse.service.entity.Pipeline;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Getter
@Setter
@Accessors(chain = true)
@Slf4j
public class AbstractDataConsumer implements DataConsumer {
    private Pipeline pipeline;

    @Override
    public void consume(DataMessage dataMessage) {
        log.info("#20 dataMessage=" + dataMessage + ", pipeline=" + pipeline);
    }
}