package org.budo.warehouse.logic.api;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lmw
 */
@Getter
@Setter
public class AbstractDataConsumerWrapper extends AbstractDataConsumer {
    private DataConsumer dataConsumer;

    @Override
    public void consume(DataMessage dataMessage) {
        this.getDataConsumer().consume(dataMessage);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + this.getDataConsumer() + ", " + this.getPipeline();
    }
}