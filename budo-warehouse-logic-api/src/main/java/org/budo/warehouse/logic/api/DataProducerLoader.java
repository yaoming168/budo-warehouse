package org.budo.warehouse.logic.api;

import org.budo.warehouse.service.entity.DataNode;

/**
 * @author lmw
 */
public interface DataProducerLoader {
    void loadDataProducer();

    void mysqlProducer(DataNode dataNode);

    String mysqlDataProducerBeanName(DataNode dataNode);
}