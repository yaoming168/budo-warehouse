package org.budo.warehouse.logic.api;

import java.util.List;
import java.util.Map;

/**
 * @author lmw
 */
public interface ILogPositionLogic {
    List<Map<String, Object>> listLogPosition();

    Boolean resetLogPositionByDestination(String destination);

    Boolean ha_health_check(String destination);
}