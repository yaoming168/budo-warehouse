package org.budo.warehouse.logic.consumer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.budo.support.dao.page.Page;
import org.budo.support.lang.util.ListUtil;
import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataMessage;
import org.budo.warehouse.logic.api.IEventFilterLogic;
import org.budo.warehouse.logic.bean.LogicDynamicBeanProvider;
import org.budo.warehouse.logic.util.DataMessageLogicUtil;
import org.budo.warehouse.service.api.IPipelineService;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
@Component("dispatcherDataConsumer")
public class DispatcherDataConsumer implements DataConsumer {
    private Map<Integer, DataConsumer> _consumerCacheMap = new ConcurrentHashMap<Integer, DataConsumer>();

    @Resource
    private IPipelineService pipelineService;

    @Resource
    private LogicDynamicBeanProvider logicDynamicBeanProvider;

    @Resource
    private IEventFilterLogic eventFilterLogic;

    @Override
    public void consume(DataMessage dataMessage) {
        Integer dataNodeId = dataMessage.getDataNodeId();
        List<Pipeline> pipelines = pipelineService.listBySourceDataNodeCached(dataNodeId, Page.max());

        if (null == pipelines || pipelines.isEmpty()) {
            log.error("#40 pipelines=" + pipelines + ", dataMessage=" + DataMessageLogicUtil.toSimpleString(dataMessage));
            return;
        }

        for (Pipeline pipeline : pipelines) {
            DataMessage filteredMessage = eventFilterLogic.filterDataMessage(pipeline, dataMessage);
            if (null == filteredMessage || ListUtil.isNullOrEmpty(filteredMessage.getDataEntries())) {
                continue; // 事件过滤
            }

            this.dispatchConsume(pipeline, filteredMessage);
        }
    }

    private void dispatchConsume(Pipeline pipeline, DataMessage dataMessage) {
        DataConsumer dataConsumer = this.getDataConsumer(pipeline);
        if (null == dataConsumer) {
            log.error("#58 dataConsumer=" + dataConsumer + ", pipeline=" + pipeline);
            return;
        }

        dataConsumer.consume(dataMessage);
    }

    private DataConsumer getDataConsumer(Pipeline pipeline) {
        DataConsumer _consumer = _consumerCacheMap.get(pipeline.getId());
        if (null != _consumer) {
            return _consumer;
        }

        synchronized (DispatcherDataConsumer.class) {
            _consumer = _consumerCacheMap.get(pipeline.getId());
            if (null != _consumer) {
                return _consumer;
            }
        }

        _consumer = logicDynamicBeanProvider.dataConsumer(pipeline);
        _consumerCacheMap.put(pipeline.getId(), _consumer);
        return _consumer;
    }
}