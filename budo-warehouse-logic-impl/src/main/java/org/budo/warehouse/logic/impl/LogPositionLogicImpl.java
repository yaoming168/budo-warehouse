package org.budo.warehouse.logic.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.budo.druid.util.DruidUtil;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.javax.sql.util.ResultSetUtil;
import org.budo.support.lang.util.ListUtil;
import org.budo.support.lang.util.MapUtil;
import org.budo.support.lang.util.NumberUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.logic.api.ILogPositionLogic;
import org.budo.warehouse.logic.producer.BudoAvailableManager;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.stereotype.Component;

import com.alibaba.otter.canal.parse.index.CanalLogPositionManager;
import com.alibaba.otter.canal.protocol.position.EntryPosition;
import com.alibaba.otter.canal.protocol.position.LogPosition;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Component
public class LogPositionLogicImpl implements ILogPositionLogic {
    @Resource
    private BudoAvailableManager budoAvailableManager;

    @Resource
    private DataProducerLoader dataProducerLoader;

    @Resource
    private CanalLogPositionManager logPositionManager;

    @Override
    public Boolean ha_health_check(String destination) {
        List<DataNode> dataNodeList = budoAvailableManager.listDataNode();
        for (DataNode dataNode : dataNodeList) {
            String _destination = dataProducerLoader.mysqlDataProducerBeanName(dataNode);
            if (!StringUtil.equals(destination, _destination)) {
                continue;
            }

            String sql = " CREATE TABLE IF NOT EXISTS `mysql`.`ha_health_check` ( `id` int(11) ) ";
            Connection connection = JdbcUtil.getConnection(dataNode.getUrl(), dataNode.getUsername(), dataNode.getPassword());
            JdbcUtil.executeUpdate(connection, sql, null);
        }

        return true;
    }

    @Override
    public Boolean resetLogPositionByDestination(String destination) {
        List<DataNode> dataNodeList = budoAvailableManager.listDataNode();
        for (DataNode dataNode : dataNodeList) {
            String _destination = dataProducerLoader.mysqlDataProducerBeanName(dataNode);
            if (!StringUtil.equals(destination, _destination)) {
                continue;
            }

            logPositionManager.persistLogPosition(destination, new LogPosition());
        }

        return true;
    }

    @Override
    public List<Map<String, Object>> listLogPosition() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        List<DataNode> dataNodeList = budoAvailableManager.listDataNode();
        for (DataNode dataNode : dataNodeList) {
            String _destination = dataProducerLoader.mysqlDataProducerBeanName(dataNode);

            LogPosition masterStatus = this.masterStatus(dataNode);

            Map<String, Object> map = MapUtil.stringObjectMap( //
                    "destination", _destination, //
                    "serverId", null, //
                    "slaveId", null, //
                    "masterAddress", dataNode.getUrl(), //
                    "masterUsername", dataNode.getUsername(), //
                    "masterStatus", masterStatus);

            LogPosition slavePosition = logPositionManager.getLatestIndexBy(_destination);
            map.put("slavePosition", slavePosition);

            list.add(map);
        }

        return list;
    }

    private LogPosition masterStatus(DataNode dataNode) {
        log.info("#156 dataNode=" + dataNode);

        Connection connection = JdbcUtil.getConnection(dataNode.getUrl(), dataNode.getUsername(), DruidUtil.rsaDecrypt(dataNode.getPassword()));
        PreparedStatement preparedStatement = JdbcUtil.prepareStatement(connection, "SHOW MASTER STATUS");
        ResultSet resultSet = JdbcUtil.executeQuery(preparedStatement);

        List<Map<String, Object>> masterStatus = ResultSetUtil.toMapList(resultSet, false);

        JdbcUtil.close(resultSet);
        JdbcUtil.close(preparedStatement);
        JdbcUtil.close(connection);

        Map<String, Object> _masterStatus = ListUtil.first(masterStatus);
        String journalName = StringUtil.toString(_masterStatus.get("File"));
        Long position = NumberUtil.toLong(_masterStatus.get("Position"));

        LogPosition logPosition = new LogPosition();
        logPosition.setPostion(new EntryPosition(journalName, position));
        return logPosition;
    }
}