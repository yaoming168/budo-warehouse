package org.budo.warehouse.logic.producer;

import java.net.InetSocketAddress;

import javax.annotation.Resource;

import org.budo.druid.util.DruidUtil;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.entity.DataNode;

import com.alibaba.otter.canal.parse.index.CanalLogPositionManager;
import com.alibaba.otter.canal.parse.support.AuthenticationInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Getter
@Setter
public abstract class AbstractBinLogClient implements BinLogClient {
    @Resource(name = "logPositionManager")
    private CanalLogPositionManager logPositionManager;

    @Resource(name = "dispatcherDataConsumer")
    private DataConsumer dispatcherDataConsumer;

    @Resource
    private IDataNodeService dataNodeService;

    @Resource
    private DataProducerLoader dataProducerLoader;

    private Integer dataNodeId;

    private String _destination;

    private AuthenticationInfo _authenticationInfo;

    public String getDestination() {
        if (null != this._destination) {
            return this._destination;
        }

        DataNode dataNode = this.getDataNodeService().findById(this.getDataNodeId());
        return this._destination = this.getDataProducerLoader().mysqlDataProducerBeanName(dataNode);
    }

    public AuthenticationInfo getAuthenticationInfo() {
        if (null != this._authenticationInfo) {
            return this._authenticationInfo;
        }

        DataNode dataNode = this.getDataNodeService().findById(this.getDataNodeId());

        String host = JdbcUtil.getHost(dataNode.getUrl());
        Integer port = JdbcUtil.getPort(dataNode.getUrl());
        port = null == port ? 3306 : port;

        String username = dataNode.getUsername();

        String password = dataNode.getPassword();
        password = null == password ? "" : password;
        password = DruidUtil.rsaDecrypt(password); // 密码解密

        // 检查数据库可达,账号正常
        log.info("#73 checkConnectMysql, url=" + dataNode.getUrl());
        JdbcUtil.checkConnectMysql(host, port, username, password);

        return this._authenticationInfo = new AuthenticationInfo(new InetSocketAddress(host, port), username, password);
    }
}