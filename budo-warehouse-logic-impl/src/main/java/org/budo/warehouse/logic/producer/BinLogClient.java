package org.budo.warehouse.logic.producer;

/**
 * @author lmw
 */
public interface BinLogClient {
    void startInstance();

    void stopInstance();
}