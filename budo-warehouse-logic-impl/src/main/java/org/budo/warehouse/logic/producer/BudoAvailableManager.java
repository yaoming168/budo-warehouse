package org.budo.warehouse.logic.producer;

import java.util.List;

import org.budo.warehouse.service.entity.DataNode;

/**
 * @author lmw
 */
public interface BudoAvailableManager {
    List<DataNode> listDataNode();

    void startManager();

    void startDataNode(DataNode dataNode);

    void stopDataNode(DataNode dataNode);
}