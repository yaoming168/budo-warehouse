package org.budo.warehouse.logic.producer;

import java.util.List;

import javax.annotation.Resource;

import org.budo.druid.util.DruidUtil;
import org.budo.support.dao.page.Page;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.support.spring.bean.factory.support.BeanBuilder;
import org.budo.support.spring.context.aware.RootApplicationContextRefreshedEventListener;
import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.logic.bean.LogicDynamicBeanProvider;
import org.budo.warehouse.logic.producer.canal.CanalBinLogClient;
import org.budo.warehouse.logic.producer.shyiko.ShyikoBinLogClient;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.api.IPipelineService;
import org.budo.warehouse.service.entity.DataNode;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
@Getter
@Setter
@Order(Ordered.LOWEST_PRECEDENCE) // 在BudoAutoDdlSupport后
public class WarehouseProducerLoader extends RootApplicationContextRefreshedEventListener implements DataProducerLoader {
    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private IDataNodeService dataNodeService;

    @Resource
    private IPipelineService pipelineService;

    @Resource
    private LogicDynamicBeanProvider logicDynamicBeanProvider;

    /**
     * 为各种入口建立监听
     */
    @Override
    public void onRootApplicationContextRefreshedEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("#54 onRootApplicationContextRefreshedEvent, " + contextRefreshedEvent);

        this.loadDataProducer();
    }

    @Override
    public void loadDataProducer() {
        log.info("#61 loadDataProducer, this=" + this);

        List<DataNode> sourceDataNodes = dataNodeService.listSourceDataNodes(Page.max());

        for (DataNode sourceDataNode : sourceDataNodes) {
            String url = sourceDataNode.getUrl();
            log.info("#65 loadDataProducer, url=" + url);

            if (url.startsWith("jdbc:mysql://")) {
                // this.mysqlDataProducer(sourceDataNode); // will init by AvailableManager
                continue;
            }

            if (url.startsWith("async:")) { // 192.168.4.253:9092 tcp://192.168.4.251:61616
                List<Pipeline> pipelines = pipelineService.listBySourceDataNode(sourceDataNode.getId(), Page.max());
                for (Pipeline pipeline : pipelines) {
                    logicDynamicBeanProvider.asyncDataProducer(sourceDataNode, pipeline);
                }
                continue;
            }

            throw new RuntimeException("#80 不支持的数据源头节点, url=" + url + ", sourceDataNode=" + sourceDataNode);
        }
    }

    @Override
    public void mysqlProducer(DataNode dataNode) {
        String engine = dataNode.getEngine();
        if (null == engine || engine.trim().isEmpty()) { // 默认
            this.shyikoProducer(dataNode);
            return;
        }

        if (StringUtil.equalsIgnoreCase("shyiko", engine)) { // 指定
            this.shyikoProducer(dataNode);
            return;
        }

        if (StringUtil.equalsIgnoreCase("canal", engine)) {
            this.canalProducer(dataNode);
            return;
        }

        throw new RuntimeException("#105 engine=" + engine + ", dataNode=" + dataNode);
    }

    private void canalProducer(DataNode dataNode) {
        String beanId = this.mysqlDataProducerBeanName(dataNode);

        if (this.getApplicationContext().containsBean(beanId)) {
            log.warn("#103 bean exists, return, beanId=" + beanId);
            return;
        }

        String host = JdbcUtil.getHost(dataNode.getUrl());
        Integer port = JdbcUtil.getPort(dataNode.getUrl());
        port = null == port ? 3306 : port;

        String username = dataNode.getUsername();

        String password = dataNode.getPassword();
        password = null == password ? "" : password;
        password = DruidUtil.rsaDecrypt(password); // 密码解密

        // 检查数据库可达,账号正常
        log.info("#113 checkConnectMysql, url=" + dataNode.getUrl());
        JdbcUtil.checkConnectMysql(host, port, username, password);

        new BeanBuilder() //
                .id(beanId) //
                .type(CanalBinLogClient.class) //
                .property("dataNodeId", dataNode.getId()) //
                .registerTo(this.getApplicationContext());
    }

    private void shyikoProducer(DataNode dataNode) {
        String beanId = this.mysqlDataProducerBeanName(dataNode);

        if (this.getApplicationContext().containsBean(beanId)) {
            log.warn("#103 bean exists, return, beanId=" + beanId);
            return;
        }

        new BeanBuilder() //
                .id(beanId) //
                .type(ShyikoBinLogClient.class) //
                .property("dataNodeId", dataNode.getId()) //
                .registerTo(this.getApplicationContext());
    }

    @Override
    public String mysqlDataProducerBeanName(DataNode dataNode) {
        String beanId = "binlog-" + dataNode.getId() + "-" + dataNode.getUrl();
        beanId = beanId.replaceAll(":", "");
        beanId = beanId.replaceAll("\\.", "");
        beanId = beanId.replaceAll("//", "");
        return beanId;
    }
}