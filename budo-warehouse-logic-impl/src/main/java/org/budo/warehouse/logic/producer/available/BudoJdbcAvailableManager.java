package org.budo.warehouse.logic.producer.available;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.sql.DataSource;

import org.budo.support.java.net.util.JavaNetUtil;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.lang.util.ProcessUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.time.Time;
import org.budo.time.TimePoint;
import org.budo.warehouse.logic.producer.BudoAvailableManager;
import org.budo.warehouse.service.entity.DataNode;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Getter
@Setter
public abstract class BudoJdbcAvailableManager implements BudoAvailableManager {
    private static final int DELAY = 2 * 1000;

    private static final int PERIOD = 2 * 1000;

    private DataSource dataSource;

    // 启动定时器
    @Override
    public void startManager() {
        Timer timer = new Timer("BudoJdbcAvailableManager-Timer-" + ProcessUtil.getCurrentProcessName());
        TimerTask timerTask = new TimerTask() {
            public void run() {
                try {
                    _checkRunning_();
                } catch (Throwable e) {
                    log.error("#63 checkRunning_0 error, e=" + e, e);
                }
            }
        };

        log.info("#49 startManager, timer=" + timer + ", timerTask=" + timerTask + ", this=" + this);
        timer.schedule(timerTask, DELAY, PERIOD);
    }

    private void _checkRunning_() {
        List<Map<String, Object>> runningMonitorList = this.listRunningMonitor();

        List<DataNode> mysqlSourceDataNodes = this.listDataNode();

        for (DataNode dataNode : mysqlSourceDataNodes) {
            if (this.shouldStart(dataNode, runningMonitorList)) {
                this.startDataNode(dataNode);
            }

            if (this.shouldStop(dataNode, runningMonitorList)) {
                this.stopDataNode(dataNode);
            }
        }
    }

    private boolean shouldStart(DataNode dataNode, List<Map<String, Object>> runningMonitorList) {
        String meInfo = ProcessUtil.getCurrentProcessId() + "@" + JavaNetUtil.getHostInnerFirst_1();
        String destination = this.destinationName(dataNode);

        Map<String, Object> runningMonitor = this.findByDestination(dataNode, runningMonitorList);
        if (null == runningMonitor) { // 未启动
            this.insertRunningMonitor(meInfo, destination);
            return true; // 启动
        }

        String running = runningMonitor.get("running") + "";
        if (StringUtil.equalsIgnoreCase(running, meInfo)) { // 就是我
            this.updateRunningMonitorTime(destination);
            return false; // 不启动
        }

        // 是别人
        TimePoint updatedAt = Time.when(runningMonitor.get("updated_at") + "", "yyyy-MM-dd HH:mm:ss");
        TimePoint max = Time.now().plusMilliSecond(PERIOD * -3); // 3倍周期之前

        if (updatedAt.isBefore(max)) { // 但好像死了
            this.updateRunningMonitor(meInfo, destination);
            return true; // 启动
        }

        // 是别人，而且挺好的
        return false; // 不启动
    }

    private boolean shouldStop(Object dataNode, List<Map<String, Object>> runningMonitorList) {
        return false;
    }

    private List<Map<String, Object>> listRunningMonitor() {
        String sql = "SELECT * FROM `t_running_monitor`";
        List<Map<String, Object>> runningMonitorList = JdbcUtil.executeQuery(this.getDataSource(), sql);
        return runningMonitorList;
    }

    private void insertRunningMonitor(String running, String destination) {
        String sql = " INSERT INTO t_running_monitor ( `running`, `destination`, `updated_at` ) VALUES ( ?, ?, ? ) ";
        Object[] parameters = new Object[] { running, destination, new Date() };
        JdbcUtil.executeUpdate(this.getDataSource(), sql, parameters);
    }

    private void updateRunningMonitor(String running, String destination) {
        String sql = " UPDATE t_running_monitor SET updated_at=?, running=? WHERE `destination` = ? ";
        Object[] parameters = new Object[] { new Date(), running, destination };
        JdbcUtil.executeUpdate(this.getDataSource(), sql, parameters);
    }

    private void updateRunningMonitorTime(String destination) {
        String sql = " UPDATE t_running_monitor SET updated_at=? WHERE `destination` = ? ";
        Object[] parameters = new Object[] { new Date(), destination };
        JdbcUtil.executeUpdate(this.getDataSource(), sql, parameters);
    }

    private Map<String, Object> findByDestination(DataNode dataNode, List<Map<String, Object>> runningMonitorList) {
        String destinationName = this.destinationName(dataNode);

        for (Map<String, Object> runningMonitor : runningMonitorList) {
            if (StringUtil.equalsIgnoreCase(destinationName, runningMonitor.get("destination") + "")) {
                return runningMonitor;
            }
        }

        return null;
    }

    public abstract String destinationName(DataNode dataNode);
}