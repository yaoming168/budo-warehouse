package org.budo.warehouse.logic.producer.available;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.budo.support.dao.page.Page;
import org.budo.support.spring.util.SpringUtil;
import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.logic.producer.BinLogClient;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Getter
@Setter
@ToString
public class WarehouseAvailableManager extends BudoJdbcAvailableManager implements ApplicationListener<ApplicationEvent> {
    @Resource
    private DataProducerLoader dataProducerLoader;

    @Resource
    private IDataNodeService dataNodeService;

    @Resource
    private DataSource dataSource;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (!SpringUtil.isRootApplicationContextRefreshedEvent(event)) {
            return;
        }

        log.warn("#46 start AvailableManagerr, this=" + this);
        this.startManager();
    }

    @Override
    public void startDataNode(DataNode dataNode) {
        String beanName = dataProducerLoader.mysqlDataProducerBeanName(dataNode);
        BinLogClient binLogClient = (BinLogClient) SpringUtil.getBean(beanName);
        if (null == binLogClient) {
            dataProducerLoader.mysqlProducer((DataNode) dataNode);
            binLogClient = (BinLogClient) SpringUtil.getBean(beanName);
        }

        binLogClient.startInstance();
    }

    @Override
    public void stopDataNode(DataNode dataNode) {
        String beanName = dataProducerLoader.mysqlDataProducerBeanName(dataNode);
        BinLogClient binLogClient = (BinLogClient) SpringUtil.getBean(beanName);
        if (null == binLogClient) {
            return;
        }

        binLogClient.stopInstance();
    }

    @Override
    public List<DataNode> listDataNode() {
        List<DataNode> mysqlSourceDataNodes = new ArrayList<DataNode>();

        List<DataNode> sourceDataNodes = dataNodeService.listSourceDataNodes(Page.max());
        for (DataNode sourceDataNode : sourceDataNodes) {
            String url = sourceDataNode.getUrl();
            if (url.startsWith("jdbc:mysql://")) {
                mysqlSourceDataNodes.add(sourceDataNode);
            }
        }

        return mysqlSourceDataNodes;
    }

    @Override
    public String destinationName(DataNode dataNode) {
        return dataProducerLoader.mysqlDataProducerBeanName(dataNode);
    }
}