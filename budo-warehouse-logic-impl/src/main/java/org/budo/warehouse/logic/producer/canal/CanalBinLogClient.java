package org.budo.warehouse.logic.producer.canal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;
import org.budo.warehouse.logic.api.DataMessagePojo;
import org.budo.warehouse.logic.producer.AbstractBinLogClient;

import com.alibaba.otter.canal.common.alarm.LogAlarmHandler;
import com.alibaba.otter.canal.instance.core.CanalInstance;
import com.alibaba.otter.canal.instance.core.CanalInstanceGenerator;
import com.alibaba.otter.canal.instance.spring.CanalInstanceWithSpring;
import com.alibaba.otter.canal.meta.MemoryMetaManager;
import com.alibaba.otter.canal.parse.ha.HeartBeatHAController;
import com.alibaba.otter.canal.parse.inbound.mysql.rds.RdsBinlogEventParserProxy;
import com.alibaba.otter.canal.parse.support.AuthenticationInfo;
import com.alibaba.otter.canal.protocol.CanalEntry.Entry;
import com.alibaba.otter.canal.protocol.ClientIdentity;
import com.alibaba.otter.canal.protocol.Message;
import com.alibaba.otter.canal.server.embedded.CanalServerWithEmbedded;
import com.alibaba.otter.canal.sink.entry.EntryEventSink;
import com.alibaba.otter.canal.store.CanalEventStore;
import com.alibaba.otter.canal.store.memory.MemoryEventStoreWithBuffer;
import com.alibaba.otter.canal.store.model.BatchMode;
import com.alibaba.otter.canal.store.model.Event;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
@Getter
@Setter
public class CanalBinLogClient extends AbstractBinLogClient {
    private Boolean keepRunning = true;

    @Override
    public void stopInstance() {
        short clientId = 1001; // @see https://github.com/alibaba/canal/wiki/ClientAPI
        ClientIdentity clientIdentity = new ClientIdentity(this.getDestination(), clientId);
        this.canalServer().unsubscribe(clientIdentity);
        this.canalServer().stop(this.getDestination());

        this.keepRunning = false;
    }

    // 启动服务端
    public void startInstance() {
        final CanalServerWithEmbedded canalServer = this.canalServer();
        canalServer.start(this.getDestination());

        // 消费线程
        Thread thread = new Thread(new Runnable() {
            public void run() {
                subscribeAndGet(canalServer);
            }
        });

        thread.setName("CanalBinLogClient-Thread");
        thread.setDaemon(true);
        thread.start();
    }

    private void subscribeAndGet(final CanalServerWithEmbedded canalServer) {
        log.info("#69 subscribeAndGet, canalServer=" + canalServer + ", this=" + this);

        short clientId = 1001; // @see https://github.com/alibaba/canal/wiki/ClientAPI
        ClientIdentity clientIdentity = new ClientIdentity(this.getDestination(), clientId);

        canalServer.subscribe(clientIdentity); // 订阅

        while (this.keepRunning) {
            int batchSize = 4 * 1024;
            long timeout = 5 * 1000L;
            Message message = canalServer.getWithoutAck(clientIdentity, batchSize, timeout, TimeUnit.MILLISECONDS);

            this.consumeCanalMessage(message);
        }

        log.warn("#69 subscribeAndGet after while, canalServer=" + canalServer + ", this=" + this);
    }

    private CanalServerWithEmbedded canalServer() {
        final CanalServerWithEmbedded canalServer = CanalServerWithEmbedded.instance(); // 单利
        canalServer.setCanalInstanceGenerator(new CanalInstanceGenerator() {
            public CanalInstance generate(String destination) {
                return generateCanalInstance(destination);
            }
        });

        canalServer.start(); // 可重复调用
        return canalServer;
    }

    private CanalInstance generateCanalInstance(String destination) {
        // init eventStore
        CanalEventStore<Event> eventStore = this.eventStore();

        EntryEventSink eventSink = new EntryEventSink();
        eventSink.setEventStore(eventStore);

        MemoryMetaManager metaManager = new MemoryMetaManager();
        LogAlarmHandler alarmHandler = new LogAlarmHandler();

        AuthenticationInfo authenticationInfo = this.getAuthenticationInfo();
        HeartBeatHAController haController = new HeartBeatHAController();

        // init eventParser
        RdsBinlogEventParserProxy eventParser = new RdsBinlogEventParserProxy();
        eventParser.setMasterInfo(authenticationInfo);
        eventParser.setLogPositionManager(this.getLogPositionManager());
        eventParser.setHaController(haController);
        eventParser.setEventSink(eventSink);
        eventParser.setDestination(destination);

        // init canalInstance
        CanalInstanceWithSpring canalInstance = new CanalInstanceWithSpring();
        canalInstance.setMetaManager(metaManager);
        canalInstance.setAlarmHandler(alarmHandler);
        canalInstance.setEventStore(eventStore);
        canalInstance.setEventSink(eventSink);
        canalInstance.setEventParser(eventParser);

        canalInstance.setDestination(destination);

        return canalInstance;
    }

    private MemoryEventStoreWithBuffer eventStore() {
        MemoryEventStoreWithBuffer memoryEventStoreWithBuffer = new MemoryEventStoreWithBuffer();
        memoryEventStoreWithBuffer.setRaw(false); // 注意，需要 false
        memoryEventStoreWithBuffer.setDdlIsolation(true);

        memoryEventStoreWithBuffer.setBatchMode(BatchMode.MEMSIZE); // 限制内存消耗
        memoryEventStoreWithBuffer.setBufferSize(2048);// otter默认1w 单次拉消息条数

        return memoryEventStoreWithBuffer;
    }

    private void consumeCanalMessage(Message message) {
        if (null == message || null == message.getEntries() || message.getEntries().isEmpty()) {
            log.error("#125 consumeCanalMessage, dataNodeId=" + this.getDataNodeId() + ", message=" + message);
            return;
        }

        List<Entry> entries = message.getEntries();

        List<DataEntry> dataEntries = new ArrayList<DataEntry>();
        for (Entry entry : entries) {
            CanalDataEntry canalDataEntry = new CanalDataEntry(entry);
            dataEntries.add(canalDataEntry);
        }

        DataMessage dataMessage = new DataMessagePojo(message.getId(), this.getDataNodeId(), dataEntries);
        this.getDispatcherDataConsumer().consume(dataMessage);
    }
}