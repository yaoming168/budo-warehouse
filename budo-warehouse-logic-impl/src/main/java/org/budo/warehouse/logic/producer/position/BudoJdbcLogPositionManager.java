package org.budo.warehouse.logic.producer.position;

import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.util.List;

import javax.sql.DataSource;

import org.budo.jdbc.dao.ResultSetHandler;
import org.budo.jdbc.dao.impl.BudoJdbcDaoImpl;
import org.budo.jdbc.dao.resultset.handler.AbstractListResultSetHandler;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.lang.util.ListUtil;

import com.alibaba.otter.canal.parse.exception.CanalParseException;
import com.alibaba.otter.canal.parse.index.AbstractLogPositionManager;
import com.alibaba.otter.canal.protocol.position.EntryPosition;
import com.alibaba.otter.canal.protocol.position.LogIdentity;
import com.alibaba.otter.canal.protocol.position.LogPosition;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lmw
 */
@Getter
@Setter
public class BudoJdbcLogPositionManager extends AbstractLogPositionManager {
    private DataSource DataSource;

    private static final ResultSetHandler<List<LogPosition>> LOG_POSITION_RESULTSET_HANDLER = new AbstractListResultSetHandler<LogPosition>() {
        public LogPosition handleResultSetRow(ResultSet resultSet) throws Throwable {
            Long slave_id = resultSet.getLong("slave_id");
            String address = resultSet.getString("address");
            Integer port = resultSet.getInt("port");

            LogIdentity logIdentity = new LogIdentity();
            logIdentity.setSlaveId(slave_id);
            logIdentity.setSourceAddress(new InetSocketAddress(address, port));

            String gtid = resultSet.getString("gtid");
            Boolean included = resultSet.getBoolean("included");
            String journal_name = resultSet.getString("journal_name");
            Long position = resultSet.getLong("position");
            Long server_id = resultSet.getLong("server_id");
            Long timestamp = resultSet.getLong("timestamp");

            EntryPosition entryPosition = new EntryPosition();
            entryPosition.setGtid(gtid);
            entryPosition.setIncluded(included);
            entryPosition.setJournalName(journal_name);
            entryPosition.setPosition(position);
            entryPosition.setServerId(server_id);
            entryPosition.setTimestamp(timestamp);

            LogPosition logPosition = new LogPosition();
            logPosition.setIdentity(logIdentity);
            logPosition.setPostion(entryPosition);
            return logPosition;
        }
    };

    @Override
    public LogPosition getLatestIndexBy(String destination) {
        BudoJdbcDaoImpl budoJdbcDaoImpl = new BudoJdbcDaoImpl(this.getDataSource());
        String sql = "SELECT * FROM `t_log_position` WHERE `destination` = ?";
        Object[] parameters = { destination };

        List<LogPosition> list = budoJdbcDaoImpl.executeQuery(sql, parameters, LOG_POSITION_RESULTSET_HANDLER);
        return ListUtil.first(list);
    }

    @Override
    public void persistLogPosition(String destination, LogPosition logPosition) throws CanalParseException {
        Integer update = this.update(destination, logPosition);

        if (update < 1) {
            this.insert(destination, logPosition);
        }
    }

    private Integer update(String destination, LogPosition logPosition) {
        LogIdentity positionIdentity = logPosition.getIdentity();
        InetSocketAddress identityAddress = positionIdentity.getSourceAddress();
        EntryPosition entryPosition = logPosition.getPostion();

        Object[] parameters = { positionIdentity.getSlaveId(), identityAddress.getHostString(), identityAddress.getPort(), //
                entryPosition.getGtid(), entryPosition.isIncluded(), entryPosition.getJournalName(), entryPosition.getPosition(), entryPosition.getServerId(), entryPosition.getTimestamp(), //
                destination };

        String updateSql = " UPDATE `t_log_position` SET `slave_id`=?, `address`=?, `port`=?, " //
                + " `gtid`=?, `included`=?, `journal_name`=?, `position`=?, `server_id`=?, `timestamp`=? " //
                + " WHERE `destination`=? ";

        Integer executeUpdate = JdbcUtil.executeUpdate(this.getDataSource(), updateSql, parameters);
        return executeUpdate;
    }

    private void insert(String destination, LogPosition logPosition) {
        LogIdentity logPositionIdentity = logPosition.getIdentity();
        InetSocketAddress logPositionIdentityAddress = logPositionIdentity.getSourceAddress();
        EntryPosition entryPosition = logPosition.getPostion();

        Object[] parameters = { logPositionIdentity.getSlaveId(), logPositionIdentityAddress.getHostString(), logPositionIdentityAddress.getPort(), //
                entryPosition.getGtid(), entryPosition.isIncluded(), entryPosition.getJournalName(), entryPosition.getPosition(), entryPosition.getServerId(), entryPosition.getTimestamp(), //
                destination };

        String insertSql = " INSERT INTO `t_log_position` ( `slave_id`, `address`, `port`, " //
                + " `gtid`, `included`, `journal_name`, `position`, `server_id`, `timestamp`, " //
                + " `destination` ) " //
                + " VALUES ( ?, ?, ?, " //
                + " ?, ?, ?, ?, ?, ?, " //
                + " ? ) ";
        JdbcUtil.executeUpdate(this.getDataSource(), insertSql, parameters);
    }
}