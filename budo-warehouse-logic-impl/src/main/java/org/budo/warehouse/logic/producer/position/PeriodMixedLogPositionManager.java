package org.budo.warehouse.logic.producer.position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.budo.support.lang.util.ThreadUtil;
import org.budo.support.spring.util.SpringUtil;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import com.alibaba.otter.canal.parse.index.AbstractLogPositionManager;
import com.alibaba.otter.canal.parse.index.CanalLogPositionManager;
import com.alibaba.otter.canal.protocol.position.LogPosition;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Getter
@Setter
public class PeriodMixedLogPositionManager extends AbstractLogPositionManager implements ApplicationListener<ApplicationEvent> {
    private CanalLogPositionManager logPositionManager;

    private Set<String> persistTaskBuffer = Collections.synchronizedSet(new HashSet<String>());

    private Map<String, LogPosition> logPositions = new ConcurrentHashMap<String, LogPosition>();

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (!SpringUtil.isRootApplicationContextRefreshedEvent(event)) {
            return;
        }

        // 消费线程
        Runnable runnable = new Runnable() {
            public void run() {
                while (true) {
                    ThreadUtil.sleep(1000);
                    flushBuffer();
                }
            }
        };

        Thread thread = new Thread(runnable);

        thread.setName("PeriodMixedLogPositionManager-Thread");
        thread.setDaemon(true);
        thread.start();
    }

    @Override
    public LogPosition getLatestIndexBy(String destination) {
        return logPositionManager.getLatestIndexBy(destination);
    }

    @Override
    public void persistLogPosition(String destination, LogPosition logPosition) {
        logPositions.put(destination, logPosition);
        persistTaskBuffer.add(destination);
    }

    private void flushBuffer() {
        try {
            this.flushBuffer_0();
        } catch (Throwable e) {
            log.error("#67 flushBuffer_0 error, e=" + e, e);
            throw new RuntimeException(e);
        }
    }

    private void flushBuffer_0() {
        List<String> tasks = new ArrayList<String>(this.persistTaskBuffer);
        for (String destination : tasks) {
            LogPosition logPosition = logPositions.get(destination);
            logPositionManager.persistLogPosition(destination, logPosition);
            persistTaskBuffer.remove(destination);
        }
    }
}