package org.budo.warehouse.logic.producer.shyiko;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.lang.util.NumberUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.warehouse.logic.producer.AbstractBinLogClient;

import com.alibaba.otter.canal.parse.support.AuthenticationInfo;
import com.alibaba.otter.canal.protocol.position.EntryPosition;
import com.alibaba.otter.canal.protocol.position.LogIdentity;
import com.alibaba.otter.canal.protocol.position.LogPosition;
import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.BinaryLogClient.EventListener;
import com.github.shyiko.mysql.binlog.BinaryLogClient.LifecycleListener;
import com.github.shyiko.mysql.binlog.event.Event;
import com.github.shyiko.mysql.binlog.event.EventHeaderV4;
import com.github.shyiko.mysql.binlog.event.EventType;
import com.github.shyiko.mysql.binlog.event.TableMapEventData;
import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer;
import com.github.shyiko.mysql.binlog.event.deserialization.NullEventDataDeserializer;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Getter
@Setter
@ToString
public class ShyikoBinLogClient extends AbstractBinLogClient {
    private Map<Long, TableMapEventData> _tableMaps = new ConcurrentHashMap<Long, TableMapEventData>();

    private Map<String, List<Map<String, Object>>> _columnInfos = new ConcurrentHashMap<String, List<Map<String, Object>>>();

    private BinaryLogClient _binaryLogClient;

    @Override
    public void startInstance() {
        try {
            AuthenticationInfo _authenticationInfo = this.getAuthenticationInfo();

            this._binaryLogClient = new BinaryLogClient(_authenticationInfo.getAddress().getHostString(), //
                    _authenticationInfo.getAddress().getPort(), //
                    _authenticationInfo.getUsername(), //
                    _authenticationInfo.getPassword());

            this._loadLogPosition();

            // 时间处理回调
            this._binaryLogClient.registerEventListener(new EventListener() {
                public void onEvent(Event event) {
                    _onEvent(event);
                }
            });

            // 生命周期回调
            this._binaryLogClient.registerLifecycleListener(this.lifecycleListener());
            this._binaryLogClient.setEventDeserializer(this.eventDeserializer());
            this._binaryLogClient.connect(3 * 1000); // 启新线程并拉取BinLog
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stopInstance() {
        try {
            this._binaryLogClient.disconnect();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public TableMapEventData getTableMap(Long tableId) {
        TableMapEventData tableMap = this._tableMaps.get(tableId);
        if (null == tableMap) {
            log.error("#65 _tableMaps=" + this._tableMaps + ", tableId=" + tableId);
        }

        return tableMap;
    }

    public List<Map<String, Object>> getColumnInfo(String schemaName, String tableName) {
        String cacheKey = schemaName + "." + tableName;
        List<Map<String, Object>> columnInfo = _columnInfos.get(cacheKey);
        if (null != columnInfo) {
            return columnInfo;
        }

        columnInfo = this.loadColumnInfo(schemaName, tableName);
        _columnInfos.put(cacheKey, columnInfo);
        return columnInfo;
    }

    // 各类事件大致比例
    // TABLE_MAP=2036,
    // GTID=1976,
    // QUERY=1976,
    // XID=1974,
    // WRITE_ROWS=1922,
    // UPDATE_ROWS=99,
    // DELETE_ROWS=15
    // FORMAT_DESCRIPTION=1,
    // ROTATE=1,
    /**
     * @see com.github.shyiko.mysql.binlog.BinaryLogClient#updateClientBinlogFilenameAndPosition(Event)
     */
    private void _onEvent(Event event) {
        EventHeaderV4 eventHeader = event.getHeader();
        EventType eventType = eventHeader.getEventType();

        // 用在binlog_format为ROW模式下，将表的定义映射到一个数字，在行操作事件之前记录（包括：WRITE_ROWS_EVENT，UPDATE_ROWS_EVENT，DELETE_ROWS_EVENT）
        if (EventType.TABLE_MAP == eventType) {
            TableMapEventData tableMap = (TableMapEventData) event.getData();
            Long tableId = tableMap.getTableId();
            this._tableMaps.put(tableId, tableMap); // 供后面使用

            this._persistLogPosition();
            return;
        }

        if (EventType.GTID == eventType //
                || EventType.QUERY == eventType //
                || EventType.XID == eventType) {
            this._persistLogPosition();
            return;
        }

        if (EventType.WRITE_ROWS == eventType //
                || EventType.UPDATE_ROWS == eventType //
                || EventType.DELETE_ROWS == eventType //
                || EventType.EXT_UPDATE_ROWS == eventType//
                || EventType.EXT_DELETE_ROWS == eventType//
                || EventType.EXT_WRITE_ROWS == eventType) {
            ShyikoDataMessage dataMessage = new ShyikoDataMessage(event, ShyikoBinLogClient.this);
            this.getDispatcherDataConsumer().consume(dataMessage);
            this._persistLogPosition();
            return;
        }

        // FORMAT_DESCRIPTION_EVENT // 描述事件，被写在每个binlog文件的开始位置
        if (EventType.FORMAT_DESCRIPTION == eventType) {
            log.info("#99 FormatDescriptionEvent=" + event);
            this._persistLogPosition();
            return;
        }

        if (EventType.ROTATE == eventType // ROTATE_EVENT 当mysqld切换到新的binlog文件生成此事件，切换到新的binlog文件可以通过执行flush logs命令
                || EventType.ANONYMOUS_GTID == eventType //
                || EventType.PREVIOUS_GTIDS == eventType //
                || EventType.UNKNOWN == eventType //
                || EventType.HEARTBEAT == eventType) { // HEARTBEAT_LOG_EVENT // 主服务器告诉从服务器，主服务器还活着，不写入到日志文件中
            this._persistLogPosition();
            return;
        }

        // 其他事件
        log.error("#250 eventType=" + eventType + ", event=" + event);
        ShyikoDataMessage dataMessage = new ShyikoDataMessage(event, this);
        this.getDispatcherDataConsumer().consume(dataMessage);
        this._persistLogPosition();
        return;
    }

    private List<Map<String, Object>> loadColumnInfo(String schemaName, String tableName) {
        Connection connection = this._connection();
        String sql = " SELECT column_key,column_name FROM information_schema.columns WHERE table_schema = ? AND table_name = ? ";
        List<Map<String, Object>> columnInfo = JdbcUtil.executeQuery(connection, sql, new Object[] { schemaName, tableName });

        log.info("#149 loadColumnInfo, table=" + schemaName + "." + tableName + ", columnInfo=" + columnInfo);
        return columnInfo;
    }

    private void _loadLogPosition() {
        Connection connection = this._connection();
        String sql = " SHOW MASTER LOGS ";
        List<Map<String, Object>> masterLogPositionList = JdbcUtil.executeQuery(connection, sql);

        LogPosition logPosition = this.getLogPositionManager().getLatestIndexBy(this.getDestination());
        log.info("#97 _authenticationInfo=" + this.getAuthenticationInfo() + ", logPosition=" + logPosition);

        // 初次连接
        if (null == logPosition) {
            return;
        }

        // 连接到之前的位置
        for (Map<String, Object> masterLogPosition : masterLogPositionList) {
            String logName = masterLogPosition.get("Log_name") + "";
            Long fileSize = NumberUtil.toLong(masterLogPosition.get("File_size"));

            if (StringUtil.equalsIgnoreCase(logPosition.getPostion().getJournalName(), logName) //
                    && logPosition.getPostion().getPosition() < fileSize) {
                this._binaryLogClient.setBinlogFilename(logPosition.getPostion().getJournalName());
                this._binaryLogClient.setBinlogPosition(logPosition.getPostion().getPosition());
                return;
            }
        }

        // 之前的位置已不可用
        throw new RuntimeException("#118 not Available BinLogPosition, logPosition=" + logPosition + ", masterLogPositionList=" + masterLogPositionList);
    }

    /**
     * 定制一些 EventData 解析器，提升性能
     */
    private EventDeserializer eventDeserializer() {
        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setEventDataDeserializer(EventType.GTID, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.QUERY, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.XID, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.FORMAT_DESCRIPTION, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.ANONYMOUS_GTID, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.PREVIOUS_GTIDS, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.UNKNOWN, new NullEventDataDeserializer());
        eventDeserializer.setEventDataDeserializer(EventType.HEARTBEAT, new NullEventDataDeserializer());
        return eventDeserializer;
    }

    private LifecycleListener lifecycleListener() {
        return new LifecycleListener() {
            public void onConnect(BinaryLogClient client) {
                log.info("#89 onConnect, client=" + client);
            }

            public void onDisconnect(BinaryLogClient client) {
                log.info("#93 onDisconnect, client=" + client);
            }

            public void onEventDeserializationFailure(BinaryLogClient client, Exception ex) {
                log.error("#97 onEventDeserializationFailure, client=" + client + ", ex=" + ex);
            }

            public void onCommunicationFailure(BinaryLogClient client, Exception ex) {
                log.error("#101 onCommunicationFailure, client=" + client + ", ex=" + ex);
            }
        };
    }

    /**
     * 持久化 BinLogPosition
     * 
     * @see com.github.shyiko.mysql.binlog.BinaryLogClient#updateClientBinlogFilenameAndPosition(Event)
     */
    private void _persistLogPosition() {
        EntryPosition entryPosition = new EntryPosition(this._binaryLogClient.getBinlogFilename(), this._binaryLogClient.getBinlogPosition());

        LogPosition logPosition = new LogPosition();
        logPosition.setPostion(entryPosition);
        logPosition.setIdentity(new LogIdentity(this.getAuthenticationInfo().getAddress(), null));

        this.getLogPositionManager().persistLogPosition(this.getDestination(), logPosition);
    }

    private Connection _connection() {
        AuthenticationInfo _authenticationInfo = this.getAuthenticationInfo();
        String host = _authenticationInfo.getAddress().getHostString();
        int port = _authenticationInfo.getAddress().getPort();

        String url = "jdbc:mysql://" + host + ":" + port + "?connectTimeout=2000&socketTimeout=2000";
        return JdbcUtil.getConnection(url, _authenticationInfo.getUsername(), _authenticationInfo.getPassword());
    }
}