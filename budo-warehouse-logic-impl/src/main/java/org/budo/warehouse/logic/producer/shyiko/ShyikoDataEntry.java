package org.budo.warehouse.logic.producer.shyiko;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.budo.support.lang.util.StringUtil;
import org.budo.time.Time;
import org.budo.warehouse.logic.api.DataEntry;

import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
import com.github.shyiko.mysql.binlog.event.Event;
import com.github.shyiko.mysql.binlog.event.EventData;
import com.github.shyiko.mysql.binlog.event.TableMapEventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author lmw
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ShyikoDataEntry implements DataEntry {
    private Serializable[] row_1;

    private Entry<Serializable[], Serializable[]> row_2;

    private ShyikoDataMessage dataMessage;

    public ShyikoDataEntry(Serializable[] row, ShyikoDataMessage dataMessage) {
        this.row_1 = row;
        this.dataMessage = dataMessage;
    }

    public ShyikoDataEntry(Entry<Serializable[], Serializable[]> row, ShyikoDataMessage dataMessage) {
        this.row_2 = row;
        this.dataMessage = dataMessage;
    }

    @Override
    public Integer getColumnCount(Integer rowIndex) {
        List<Map<String, Object>> columnList = this._binLogClient().getColumnInfo(this.getSchemaName(), this.getTableName());
        return columnList.size();
    }

    // RDS有隐藏主键, 推过来的数据会多一列
    @Override
    public String getColumnName(Integer rowIndex, Integer columnIndex) {
        List<Map<String, Object>> columnList = this._binLogClient().getColumnInfo(this.getSchemaName(), this.getTableName());

        Map<String, Object> columnMap = columnList.get(columnIndex);
        return (String) columnMap.get("column_name");
    }

    @Override
    public Boolean getColumnIsKey(Integer rowIndex, Integer columnIndex) {
        List<Map<String, Object>> columnList = this._binLogClient().getColumnInfo(this.getSchemaName(), this.getTableName());
        Map<String, Object> columnMap = columnList.get(columnIndex);
        Object val = columnMap.get("column_key");
        return StringUtil.equalsIgnoreCase("PRI", val + "");
    }

    @Override
    public String getTableName() {
        Long tableId = this._tableId();

        TableMapEventData tableMap = this._binLogClient().getTableMap(tableId);
        if (null != tableMap) {
            return tableMap.getTable();
        }

        throw new RuntimeException("#143 tableMap=" + tableMap + ", event=" + this.getDataMessage().getEvent());
    }

    @Override
    public String getSchemaName() {
        Long tableId = this._tableId();
        TableMapEventData tableMap = this._binLogClient().getTableMap(tableId);
        if (null != tableMap) {
            return tableMap.getDatabase();
        }

        throw new RuntimeException("#154 tableMap=" + tableMap + ", event=" + this.getDataMessage().getEvent());
    }

    @Override
    public Integer getRowCount() {
        if (this._eventData() instanceof WriteRowsEventData) {
            WriteRowsEventData writeRowsEventData = (WriteRowsEventData) this._eventData();
            List<Serializable[]> rows = writeRowsEventData.getRows();
            return rows.size();
        }

        if (this._eventData() instanceof UpdateRowsEventData) {
            UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) this._eventData();
            List<Entry<Serializable[], Serializable[]>> rows = updateRowsEventData.getRows();
            return rows.size();
        }

        if (this._eventData() instanceof DeleteRowsEventData) {
            DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) this._eventData();
            List<Serializable[]> rows = deleteRowsEventData.getRows();
            return rows.size();
        }

        throw new RuntimeException("#103 this=" + this);
    }

    @Override
    public String getColumnValueBefore(Integer rowIndex, Integer columnIndex) {
        if (this._eventData() instanceof WriteRowsEventData) {
            return null;
        }

        if (this._eventData() instanceof UpdateRowsEventData) {
            UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) this._eventData();
            Serializable value = updateRowsEventData.getRows().get(rowIndex).getKey()[columnIndex];
            return this._valueToString(value, columnIndex);
        }

        if (this._eventData() instanceof DeleteRowsEventData) {
            DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) this._eventData();
            Serializable value = deleteRowsEventData.getRows().get(rowIndex)[columnIndex];
            return this._valueToString(value, columnIndex);
        }

        throw new RuntimeException("#103 this=" + this);
    }

    @Override
    public String getColumnValueAfter(Integer rowIndex, Integer columnIndex) {
        if (this._eventData() instanceof WriteRowsEventData) {
            WriteRowsEventData writeRowsEventData = (WriteRowsEventData) this._eventData();
            Serializable value = writeRowsEventData.getRows().get(rowIndex)[columnIndex];
            return this._valueToString(value, columnIndex);
        }

        if (this._eventData() instanceof UpdateRowsEventData) {
            UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) this._eventData();
            Serializable value = updateRowsEventData.getRows().get(rowIndex).getValue()[columnIndex];
            return this._valueToString(value, columnIndex);
        }

        if (this._eventData() instanceof DeleteRowsEventData) {
            return null;
        }

        throw new RuntimeException("#154 this=" + this);
    }

    @Override
    public String getSql() {
        EventData _eventData = this._eventData();
        if (_eventData instanceof WriteRowsEventData) {
            return null;
        }
        if (_eventData instanceof UpdateRowsEventData) {
            return null;
        }
        if (_eventData instanceof DeleteRowsEventData) {
            return null;
        }

        throw new RuntimeException("#82 _eventData=" + _eventData);
    }

    @Override
    public String getEventType() {
        EventData _eventData = this._eventData();
        if (_eventData instanceof WriteRowsEventData) {
            return EventType.INSERT;
        }
        if (_eventData instanceof UpdateRowsEventData) {
            return EventType.UPDATE;
        }
        if (_eventData instanceof DeleteRowsEventData) {
            return EventType.DELETE;
        }

        throw new RuntimeException("#70 _eventData=" + _eventData);
    }

    @Override
    public Long getExecuteTime() {
        throw new RuntimeException("#149 this=" + this);
    }

    private EventData _eventData() {
        Event _event = this.getDataMessage().getEvent();
        return _event.getData();
    }

    private ShyikoBinLogClient _binLogClient() {
        ShyikoDataMessage _dataMessage = this.getDataMessage();
        return _dataMessage.getBinLogClient();
    }

    private long _tableId() {
        EventData _eventData = this._eventData();
        if (_eventData instanceof WriteRowsEventData) {
            return ((WriteRowsEventData) _eventData).getTableId();
        }
        if (_eventData instanceof UpdateRowsEventData) {
            return ((UpdateRowsEventData) _eventData).getTableId();
        }
        if (_eventData instanceof DeleteRowsEventData) {
            return ((DeleteRowsEventData) _eventData).getTableId();
        }

        throw new RuntimeException("#70 _eventData=" + _eventData);
    }

    private String _valueToString(Serializable val, Integer columnIndex) {
        if (null == val) {
            return null;
        }

        if (val instanceof String) {
            return (String) val;
        }

        if (val instanceof Number) {
            return val.toString();
        }

        // 暂不支持
        if (val instanceof byte[]) {
            return null;
        }

        if (val instanceof Date) {
            return Time.when((Date) val).toString("yyyy-MM-dd HH:mm:ss.SSS");
        }

        List<Map<String, Object>> columnInfos = this._binLogClient().getColumnInfo(this.getSchemaName(), this.getTableName());
        throw new RuntimeException("#171 val.type=" + val.getClass() + ", table=" + this.getSchemaName() + "." + this.getTableName() + ", columnInfo=" + columnInfos.get(columnIndex));
    }
}
