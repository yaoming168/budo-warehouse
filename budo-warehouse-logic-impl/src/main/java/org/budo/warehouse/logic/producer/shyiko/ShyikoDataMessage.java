package org.budo.warehouse.logic.producer.shyiko;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;

import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
import com.github.shyiko.mysql.binlog.event.Event;
import com.github.shyiko.mysql.binlog.event.EventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author lmw
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
public class ShyikoDataMessage implements DataMessage {
    private Event event;

    private ShyikoBinLogClient binLogClient;

    public ShyikoDataMessage(Event event, ShyikoBinLogClient binLogClient) {
        this.event = event;
        this.binLogClient = binLogClient;
    }

    private EventData _eventData() {
        return this.getEvent().getData();
    }

    @Override
    public Long getId() {
        if (this._eventData() instanceof WriteRowsEventData //
                || this._eventData() instanceof UpdateRowsEventData //
                || this._eventData() instanceof DeleteRowsEventData) {
            return null;
        }

        throw new RuntimeException("#42 " + this._eventData());
    }

    @Override
    public Integer getDataNodeId() {
        return this.getBinLogClient().getDataNodeId();
    }

    @Override
    public List<DataEntry> getDataEntries() {
        if (this._eventData() instanceof DeleteRowsEventData) {
            DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) this._eventData();
            List<Serializable[]> rows = deleteRowsEventData.getRows();

            List<DataEntry> dataEntries = new ArrayList<DataEntry>();
            for (Serializable[] row : rows) {
                ShyikoDataEntry dataEntry = new ShyikoDataEntry(row, this);
                dataEntries.add(dataEntry);
            }
            return dataEntries;
        }

        if (this._eventData() instanceof WriteRowsEventData) {
            WriteRowsEventData writeRowsEventData = (WriteRowsEventData) this._eventData();
            List<Serializable[]> rows = writeRowsEventData.getRows();

            List<DataEntry> dataEntries = new ArrayList<DataEntry>();
            for (Serializable[] row : rows) {
                ShyikoDataEntry dataEntry = new ShyikoDataEntry(row, this);
                dataEntries.add(dataEntry);
            }
            return dataEntries;
        }

        if (this._eventData() instanceof UpdateRowsEventData) {
            UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) this._eventData();
            List<Entry<Serializable[], Serializable[]>> rows = updateRowsEventData.getRows();

            List<DataEntry> dataEntries = new ArrayList<DataEntry>();
            for (Entry<Serializable[], Serializable[]> row : rows) {
                ShyikoDataEntry dataEntry = new ShyikoDataEntry(row, this);
                dataEntries.add(dataEntry);
            }
            return dataEntries;
        }

        throw new RuntimeException("#114 eventHeader=" + this.getEvent().getHeader() + ", _eventData=" + this._eventData());
    }

    @Override
    public Integer getRowCount() {
        throw new RuntimeException("#119 " + this);
    }
}