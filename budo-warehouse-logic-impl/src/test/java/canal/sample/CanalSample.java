package canal.sample;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

import com.alibaba.otter.canal.common.alarm.LogAlarmHandler;
import com.alibaba.otter.canal.instance.core.CanalInstance;
import com.alibaba.otter.canal.instance.core.CanalInstanceGenerator;
import com.alibaba.otter.canal.instance.spring.CanalInstanceWithSpring;
import com.alibaba.otter.canal.meta.MemoryMetaManager;
import com.alibaba.otter.canal.parse.ha.HeartBeatHAController;
import com.alibaba.otter.canal.parse.inbound.mysql.rds.RdsBinlogEventParserProxy;
import com.alibaba.otter.canal.parse.index.MemoryLogPositionManager;
import com.alibaba.otter.canal.parse.support.AuthenticationInfo;
import com.alibaba.otter.canal.protocol.ClientIdentity;
import com.alibaba.otter.canal.protocol.Message;
import com.alibaba.otter.canal.server.embedded.CanalServerWithEmbedded;
import com.alibaba.otter.canal.sink.entry.EntryEventSink;
import com.alibaba.otter.canal.store.memory.MemoryEventStoreWithBuffer;

/**
 * @author lmw
 */
public class CanalSample {
    public static final String TJKP_SSW0RD = "";

    private static final InetSocketAddress ADDRESS = new InetSocketAddress("rm-bp127us7d36zxqm7v.mysql.rds.aliyuncs.com", 3306);

    private static final AuthenticationInfo MASTER_INFO = new AuthenticationInfo(ADDRESS, "tjk", TJKP_SSW0RD);

    public static void main(String[] args) {
        String destination = "destination-1";

        CanalInstanceGenerator canalInstanceGenerator = new CanalInstanceGenerator() {
            public CanalInstance generate(String destination) {

                MemoryEventStoreWithBuffer eventStore = new MemoryEventStoreWithBuffer();
                EntryEventSink eventSink = new EntryEventSink();
                eventSink.setEventStore(eventStore);

                MemoryMetaManager metaManager = new MemoryMetaManager();
                LogAlarmHandler alarmHandler = new LogAlarmHandler();

                MemoryLogPositionManager logPositionManager = new MemoryLogPositionManager();

                RdsBinlogEventParserProxy eventParser = new RdsBinlogEventParserProxy();
                eventParser.setMasterInfo(MASTER_INFO);
                eventParser.setLogPositionManager(logPositionManager);
                eventParser.setHaController(new HeartBeatHAController());
                eventParser.setEventSink(eventSink);
                eventParser.setDestination(destination);

                CanalInstanceWithSpring canalInstance = new CanalInstanceWithSpring();
                canalInstance.setMetaManager(metaManager);
                canalInstance.setAlarmHandler(alarmHandler);
                canalInstance.setEventStore(eventStore);
                canalInstance.setEventSink(eventSink);
                canalInstance.setEventParser(eventParser);

                canalInstance.setDestination(destination);

                return canalInstance;
            }
        };

        // 启动服务端
        final CanalServerWithEmbedded canalServer = CanalServerWithEmbedded.instance();
        canalServer.setCanalInstanceGenerator(canalInstanceGenerator);

        canalServer.start();
        canalServer.start(destination);

        // 启动客户端
        final ClientIdentity clientIdentity = new ClientIdentity();
        clientIdentity.setClientId((short) 12);
        clientIdentity.setDestination(destination);

        // 订阅
        canalServer.subscribe(clientIdentity);

        // 消费线程
        Thread thread = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    Message message = canalServer.getWithoutAck(clientIdentity, 1, 1L, TimeUnit.SECONDS);
                    System.err.println(message);
                }
            }
        });

        thread.setDaemon(true);
        thread.start();
    }
}