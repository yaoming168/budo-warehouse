package shyiko.sample;

import java.util.HashMap;
import java.util.Map;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.BinaryLogClient.EventListener;
import com.github.shyiko.mysql.binlog.event.Event;
import com.github.shyiko.mysql.binlog.event.EventHeaderV4;
import com.github.shyiko.mysql.binlog.event.EventType;

import canal.sample.CanalSample;

/**
 * @author lmw
 */
public class ShyikoSample {
    static int number = 0;
    static final Map<EventType, Integer> count = new HashMap<EventType, Integer>();

    public static void main(String[] args) throws Throwable {

        BinaryLogClient binaryLogClient = new BinaryLogClient("rm-bp127us7d36zxqm7v.mysql.rds.aliyuncs.com", 3306, "tjk", CanalSample.TJKP_SSW0RD);
        binaryLogClient.registerEventListener(new EventListener() {
            public void onEvent(Event event) {
                EventHeaderV4 eventHeader = event.getHeader();
                EventType eventType = eventHeader.getEventType();
                Integer val = count.get(eventType);
                if (null != val) {
                    count.put(eventType, val + 1);
                } else {
                    count.put(eventType, 1);
                }

                plusOne();
            }
        });
        binaryLogClient.connect();
    }

    protected static void plusOne() {
        number++;

        if (number > 9999) {
            System.err.println(count);
            System.exit(0);
        }
    }
}