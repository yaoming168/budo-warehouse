package org.budo.warehouse.service.api;

import java.util.List;

import org.budo.warehouse.service.entity.RunningMonitor;

/**
 * @author lmw
 */
public interface IRunningMonitorService {
    List<RunningMonitor> listRunningMonitor();
}