package org.budo.warehouse.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author lmw
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
@Table(name = "t_running_monitor")
public class RunningMonitor implements Serializable {
    private static final long serialVersionUID = 9124058877131685439L;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String running;

    @Column
    private String destination;

    @Column(name = "updated_at")
    private Timestamp updatedAt;
}