package org.budo.warehouse.dao.api;

import java.util.List;

import org.budo.warehouse.service.entity.RunningMonitor;

/**
 * @author lmw
 */
public interface IRunningMonitorDao {
    List<RunningMonitor> listRunningMonitor();
}