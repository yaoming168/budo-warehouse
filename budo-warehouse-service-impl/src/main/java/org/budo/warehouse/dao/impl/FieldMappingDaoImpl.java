package org.budo.warehouse.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.budo.mybatis.dao.MybatisDao;
import org.budo.support.dao.page.Page;
import org.budo.support.lang.util.MapUtil;
import org.budo.warehouse.dao.api.IFieldMappingDao;
import org.budo.warehouse.service.entity.FieldMapping;
import org.springframework.stereotype.Repository;

/**
 * @author lmw
 */
@Repository
public class FieldMappingDaoImpl implements IFieldMappingDao {
    @Resource
    private MybatisDao mybatisDao;

    @Override
    public List<FieldMapping> listByPipelineId(Integer pipelineId) {
        String sql = " SELECT * FROM t_field_mapping WHERE field_name NOT LIKE '" + FieldMapping.PREFIX + "%' " //
                + " AND pipeline_id=#{pipelineId} " //
                + " AND (deleted_at IS NULL OR deleted_at='') ";
        Map<String, Object> parameter = MapUtil.stringObjectMap("pipelineId", pipelineId);
        return mybatisDao.listBySql(FieldMapping.class, sql, parameter, Page.max());
    }

    @Override
    public Boolean findOriginalFieldsValueByPipelineId(Integer pipelineId) {
        String sql = " SELECT field_value FROM t_field_mapping WHERE pipeline_id=#{pipelineId} " //
                + " AND field_name='" + FieldMapping.ORIGINAL_FIELDS //
                + "' AND (deleted_at IS NULL OR deleted_at='') ";
        Map<String, Object> parameter = MapUtil.stringObjectMap("pipelineId", pipelineId);
        return mybatisDao.findBySql(Boolean.class, sql, parameter);
    }

    @Override
    public List<Integer> listCountByPipelineIds(List<Integer> pipelineIds) {
        List<Integer> list = new ArrayList<Integer>();
        for (Integer dataNodeId : pipelineIds) {
            Integer count = this.countByPipelineId(dataNodeId);
            list.add(count);
        }
        return list;
    }

    private Integer countByPipelineId(Integer pipelineId) {
        String sql = " SELECT COUNT(*) FROM t_field_mapping WHERE pipeline_id=#{pipelineId} ";
        Map<String, Object> parameter = MapUtil.stringObjectMap("pipelineId", pipelineId);
        return mybatisDao.findBySql(Integer.class, sql, parameter);
    }
}