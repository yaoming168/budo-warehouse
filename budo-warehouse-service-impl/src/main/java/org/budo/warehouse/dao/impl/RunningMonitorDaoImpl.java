package org.budo.warehouse.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.mybatis.dao.MybatisDao;
import org.budo.support.dao.page.Page;
import org.budo.warehouse.dao.api.IRunningMonitorDao;
import org.budo.warehouse.service.entity.RunningMonitor;
import org.springframework.stereotype.Repository;

/**
 * @author lmw
 */
@Repository
public class RunningMonitorDaoImpl implements IRunningMonitorDao {
    @Resource
    private MybatisDao mybatisDao;

    @Override
    public List<RunningMonitor> listRunningMonitor() {
        return mybatisDao.list(RunningMonitor.class, Page.max());
    }
}