package org.budo.warehouse.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.warehouse.dao.api.IRunningMonitorDao;
import org.budo.warehouse.service.api.IRunningMonitorService;
import org.budo.warehouse.service.entity.RunningMonitor;
import org.springframework.stereotype.Service;

/**
 * @author lmw
 */
@Service
public class RunningMonitorServiceImpl implements IRunningMonitorService {
    @Resource
    private IRunningMonitorDao runningMonitorDao;

    @Override
    public List<RunningMonitor> listRunningMonitor() {
        return runningMonitorDao.listRunningMonitor();
    }
}