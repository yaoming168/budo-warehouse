import java.sql.Connection;
import java.util.List;

import org.budo.druid.filter.factory.DefaultDruidFiltersFactoryBean;
import org.budo.support.javax.sql.util.JdbcUtil;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;

/**
 * @author lmw
 * @see com.mysql.jdbc.ConnectionImpl#setTransactionIsolation(int)
 */
public class 写数据速度测试 {
    private static String url = "jdbc:mysql://192.168.4.42";

    private static String username = "root";

    private static String password = "admin";

    public static void main(String[] args) throws Throwable {
        DruidDataSource druidDataSource = _druidDataSource();

        String sql = "INSERT INTO `tj_qc_v2`.`tj_quality_data_call_ext_0` (`id`, `call_id`, `field_name`, `field_desc`, `field_value`, `lrsj`) " //
                + " VALUES (?, ?, ?, ?, ?, ?) " //
                + " ON DUPLICATE KEY UPDATE `id` = ?, `call_id` = ?, `field_name` = ?, `field_desc` = ?, `field_value` = ?, `lrsj` = ?";

        // 500 条耗时 31612
        // 500 条耗时 41601
        int from = -2000;
        int count = 500;
        int to = from - count;

        long start = System.currentTimeMillis();
        for (int i = from; i > to; i--) {
            Connection connection = druidDataSource.getConnection();
            connection.setTransactionIsolation(999);
            Object[] parameters = parameters(i);

            int a = JdbcUtil.executeUpdate(connection, sql, parameters);
            System.err.println(i + " > " + a);
            connection.close();
        }
        long end = System.currentTimeMillis();
        System.err.println(end - start);
    }

    private static Object[] parameters(int id) {
        return new Object[] { //
                id, "300000000018675925", "hangupContent", "线下办理意向", "拒绝办理——不需要", "2019-04-01 09:40:18", //
                id, "300000000018675925", "hangupContent", "线下办理意向", "拒绝办理——不需要", "2019-04-01 09:40:18"//
        };
    }

    private static DruidDataSource _druidDataSource() throws Throwable {
        DruidDataSource druidDataSource = new DruidDataSource();

        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        druidDataSource.setMaxActive(25);
        druidDataSource.setMaxWait(5000);

        List<Filter> filters = new DefaultDruidFiltersFactoryBean().getObject();
        druidDataSource.setProxyFilters(filters);
        return druidDataSource;
    }
}