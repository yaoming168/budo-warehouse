import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'

// jquery
import $ from 'jquery'
window.$ = $;

Vue.config.productionTip = false

import { Loading } from 'element-ui';

let loadingCount = 0;
let loadingService;

const showLoading = () => {
  if ( window._doNotShowLoading ) {
    return;
  }

  if ( loadingCount === 0) {
    loadingService = Loading.service({ text: '加载中 ...', background: 'rgba(0, 0, 0, 0.1)'});
  }

  loadingCount += 1;
};

const hideLoading = () => {
  if(loadingCount <= 0) {
    return;
  }

  loadingCount -= 1;
  if(loadingCount === 0) {
    loadingService.close();
  }
};

// init sdk
var _sdk = new BudoDubboHttpApiJavascriptSdk();
_sdk.setUrl("/api");


_sdk.setCallback({
  success : function(a, b, c, d) {
    console.log("success", a, b, c, d);
  },
  fail : function(a, b, c, d) {
    console.log("fail", a, b, c, d);
  }
});


_sdk.setRequestFilter(function(_request) {
  showLoading();

  return _request;
});

_sdk.setResponseHandler(function(_request) {
  hideLoading();

  // 有 error
  if( !!_request.error ) {
    _request.callback.fail(_request.error.error_description, _request);
    return;
  }

  var _responseBody = _request.responseBody;
  var _responseEntity =  _request.serialization.deserialize(_responseBody);

  // 正常
  if ( "0" == _responseEntity.status + "" ) { // 数字 0 为 false
    _request.callback.success(_responseEntity.result, _request);
    return;
  }

  // 未登录
  if( _responseEntity.status == 1103 || _responseEntity.status == 1102 ) { // 未登录
    location.href = '/sso/authorize';
    return;
  }

  // 其他错误回调
  _request.callback.fail(_responseEntity.result.error_description, _request);
});

window.sdk = _sdk;

// vue
new Vue({
  router,
  render: h => h(App)
}).$mount('#app');