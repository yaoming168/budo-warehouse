package org.budo.warehouse.web.api;

import java.util.List;
import java.util.Map;

/**
 * @author lmw
 */
public interface CanalThreadApi {
    Boolean open_debug_log(String threadName);

    List<Map<String, Object>> listCanalThread();
}