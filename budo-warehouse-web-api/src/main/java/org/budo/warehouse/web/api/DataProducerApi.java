package org.budo.warehouse.web.api;

/**
 * @author lmw
 */
public interface DataProducerApi {
    void loadDataProducer();
}