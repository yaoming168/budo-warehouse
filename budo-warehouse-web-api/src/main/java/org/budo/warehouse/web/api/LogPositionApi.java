package org.budo.warehouse.web.api;

import java.util.List;
import java.util.Map;

import org.budo.dubbo.protocol.http.authentication.AuthenticationCheck;

/**
 * @author lmw
 */
@AuthenticationCheck
public interface LogPositionApi {
    Boolean ha_health_check(String destination);

    Boolean resetLogPositionByDestination(String destination);

    List<Map<String, Object>> listLogPosition();
}