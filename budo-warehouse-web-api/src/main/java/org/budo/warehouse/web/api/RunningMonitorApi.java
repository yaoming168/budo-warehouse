package org.budo.warehouse.web.api;

import java.util.List;

import org.budo.dubbo.protocol.http.authentication.AuthenticationCheck;
import org.budo.warehouse.service.entity.RunningMonitor;

/**
 * @author lmw
 */
@AuthenticationCheck
public interface RunningMonitorApi {
    List<RunningMonitor> listRunningMonitor();
}