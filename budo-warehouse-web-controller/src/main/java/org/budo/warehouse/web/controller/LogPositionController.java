package org.budo.warehouse.web.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.budo.time.Time;
import org.budo.time.TimePoint;
import org.budo.warehouse.logic.api.ILogPositionLogic;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.otter.canal.protocol.position.EntryPosition;
import com.alibaba.otter.canal.protocol.position.LogPosition;

/**
 * @author lmw
 */
@Controller
public class LogPositionController {
    private static final String _FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    @Resource
    private ILogPositionLogic logPositionLogic;

    @ResponseBody
    @RequestMapping("log_position")
    public String logPosition() {
        List<Map<String, Object>> list = logPositionLogic.listLogPosition();

        TimePoint now = Time.now();

        String html = "<table border=\"1\">";

        html += "<tr>";
        html += "<td colspan=\"" + 3 + "\"></td>";
        html += "<td colspan=\"" + 4 + "\"></td>";
        html += "<td colspan=\"" + 3 + "\">" + now.toString(_FORMAT) + "</td>";
        html += "</tr>";
        html += "<tr>";

        html += "<td>DESTINATION</td>";
        html += "<td>MASTER</td>";
        html += "<td>POSITION</td>";
        html += "<td>SERVER_ID</td>";
        html += "<td>SLAVE_ID</td>";
        html += "<td>JOURNAL</td>";
        html += "<td>POSITION</td>";
        html += "<td>TIMESTAMP</td>";
        html += "<td>DIFF POSITION</td>";
        html += "<td>DIFF TIME</td>";
        html += "</tr>";

        for (Map<String, Object> map : list) {
            Object destination = map.get("destination");
            Object serverId = map.get("serverId");
            Object slaveId = map.get("slaveId");

            EntryPosition masterStatus = (EntryPosition) map.get("masterStatus");

            Long masterPosition = null == masterStatus ? null : masterStatus.getPosition();

            html += "<tr>";
            html += "<td>" + destination + "</td>";
            html += "<td>" + (null == masterStatus ? null : masterStatus.getJournalName()) + "</td>";
            html += "<td>" + masterPosition + "</td>";
            html += "<td>" + serverId + "</td>";
            html += "<td>" + slaveId + "</td>";

            LogPosition slavePosition = (LogPosition) map.get("slavePosition");

            if (null == slavePosition) {
                html += "<td colspan=\"5\"></td>";
            } else {
                EntryPosition postion = slavePosition.getPostion();

                html += "<td>" + postion.getJournalName() + "</td>";
                html += "<td>" + postion.getPosition() + "</td>";

                TimePoint slaveTime = Time.when(postion.getTimestamp());
                html += "<td>" + slaveTime.toString(_FORMAT) + "</td>";

                html += "<td>" + (masterPosition - postion.getPosition()) + "</td>";
                html += "<td>" + now.between(slaveTime).toMilliSeconds() + "</td>";
            }

            html += "</tr>";
        }

        html += "</table>";
        return html;
    }

    @ResponseBody
    @RequestMapping("log_position.json")
    public String logPositionJson() {
        List<Map<String, Object>> list = logPositionLogic.listLogPosition();

        return JSON.toJSONString(list);
    }
}