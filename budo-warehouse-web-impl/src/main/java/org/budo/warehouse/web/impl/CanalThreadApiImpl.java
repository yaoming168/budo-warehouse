package org.budo.warehouse.web.impl;

import java.lang.management.ThreadInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.budo.support.lang.util.MapUtil;
import org.budo.support.lang.util.ProcessUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.warehouse.web.api.CanalThreadApi;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Component
public class CanalThreadApiImpl implements CanalThreadApi {
    @Override
    public List<Map<String, Object>> listCanalThread() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        ThreadInfo[] threadInfos = ProcessUtil.getThreadInfos(Integer.MAX_VALUE);
        for (ThreadInfo threadInfo : threadInfos) {
            String threadName = threadInfo.getThreadName();
            if (!StringUtil.contains(threadName, "CanalGetThread")) {
                continue;
            }

            Map<String, Object> thread = MapUtil.stringObjectMap("threadName", threadName, //
                    "BlockedCount", threadInfo.getBlockedCount(), //
                    "BlockedTime", threadInfo.getBlockedTime(), //
                    "LockName", threadInfo.getLockName(), //
                    "LockOwnerId", threadInfo.getLockOwnerId(), //
                    "ThreadId", threadInfo.getThreadId(), //
                    "ThreadState", threadInfo.getThreadState(), //
                    "WaitedCount", threadInfo.getWaitedCount(), //
                    "WaitedTime", threadInfo.getWaitedTime());

            list.add(thread);
        }

        return list;
    }

    @Override
    public Boolean open_debug_log(String threadName) {
        log.info("#53 open_debug_log");
        return null;
    }
}