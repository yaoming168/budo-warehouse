package org.budo.warehouse.web.impl;

import javax.annotation.Resource;

import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataMessage;
import org.budo.warehouse.web.api.DataMessageApi;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Component
public class DataMessageApiImpl implements DataMessageApi {
    @Resource(name = "dispatcherDataConsumer")
    private DataConsumer dataConsumer;

    @Override
    public void post(DataMessage dataMessage) {
        log.info("#23 post, dataMessage=" + dataMessage);
        dataConsumer.consume(dataMessage);
    }
}