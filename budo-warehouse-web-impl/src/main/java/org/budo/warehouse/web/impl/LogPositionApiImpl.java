package org.budo.warehouse.web.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.budo.warehouse.logic.api.ILogPositionLogic;
import org.budo.warehouse.web.api.LogPositionApi;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Component
public class LogPositionApiImpl implements LogPositionApi {
    @Resource
    private ILogPositionLogic logPositionLogic;

    @Override
    public List<Map<String, Object>> listLogPosition() {
        List<Map<String, Object>> listLogPosition = logPositionLogic.listLogPosition();

        log.info("#27 listLogPosition return, " + listLogPosition);
        return listLogPosition;
    }

    @Override
    public Boolean resetLogPositionByDestination(String destination) {
        return logPositionLogic.resetLogPositionByDestination(destination);
    }

    @Override
    public Boolean ha_health_check(String destination) {
        return logPositionLogic.ha_health_check(destination);
    }
}