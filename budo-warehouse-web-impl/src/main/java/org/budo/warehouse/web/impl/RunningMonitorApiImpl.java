package org.budo.warehouse.web.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.warehouse.service.api.IRunningMonitorService;
import org.budo.warehouse.service.entity.RunningMonitor;
import org.budo.warehouse.web.api.RunningMonitorApi;
import org.springframework.stereotype.Component;

/**
 * @author lmw
 */
@Component
public class RunningMonitorApiImpl implements RunningMonitorApi {
    @Resource
    private IRunningMonitorService runningMonitorService;

    @Override
    public List<RunningMonitor> listRunningMonitor() {
        return runningMonitorService.listRunningMonitor();
    }
}